FROM python:3

RUN python3 -m pip install numpy websockify

EXPOSE 3001

ENTRYPOINT ["websockify", "-v", "3001", "--idle-timeout=3600", "prahy.mmh.cz:3333"]
